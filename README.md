# smart-fridge

### App on Heroku
[fridgesmart.herokuapp.com](https://fridgesmart.herokuapp.com/)<br /><br />
without authenticated can view:<br />
[main page](https://fridgesmart.herokuapp.com/)<br />
[Wishlist for cooking](https://fridgesmart.herokuapp.com/wishlist/)<br />
[Current fridge list](https://fridgesmart.herokuapp.com/fridge/)<br />

Other functions only for authenticated 
Login:admin
Password:admin

### Description
[Smart fridge task](https://drive.google.com/file/d/1KYp9Q1UfJ3VM8jhqvZKTsDjLyiDfuxvW/view)
An application that simulates the work of a "smart fridge",
which will analyze what products are available in the fridge
and which you need to buy more for cooking our selected recipes.