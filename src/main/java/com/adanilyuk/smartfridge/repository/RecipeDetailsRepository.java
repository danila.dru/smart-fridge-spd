package com.adanilyuk.smartfridge.repository;

import com.adanilyuk.smartfridge.entity.RecipeDetail;
import com.adanilyuk.smartfridge.entity.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RecipeDetailsRepository extends JpaRepository<RecipeDetail,Integer> {
    List<RecipeDetail> findAllByRecipeOrderById(Recipe recipe);
}
