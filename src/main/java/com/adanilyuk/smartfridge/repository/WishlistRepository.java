package com.adanilyuk.smartfridge.repository;

import com.adanilyuk.smartfridge.entity.WishList;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WishlistRepository extends JpaRepository<WishList, Integer> {
}
