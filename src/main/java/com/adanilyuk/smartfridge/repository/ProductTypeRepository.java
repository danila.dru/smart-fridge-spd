package com.adanilyuk.smartfridge.repository;

import com.adanilyuk.smartfridge.entity.ProductType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductTypeRepository extends JpaRepository<ProductType,Integer> {
}
