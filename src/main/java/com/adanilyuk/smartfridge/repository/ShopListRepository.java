package com.adanilyuk.smartfridge.repository;

import com.adanilyuk.smartfridge.entity.ShopListIdOnly;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ShopListRepository extends JpaRepository<ShopListIdOnly, Integer> {
    @Query(value = """
                   select product.id as product_id,
                   pt.name as typename,
                   product.name as productname,
                   sum(rd.amount * wl.amount) -
                   COALESCE((select sum(fridge.amount) from fridge where fridge.product_id = product.id), 0) as need
            from wishlist wl
                     inner join recipe on recipe.id = wl.recipe_id
                     inner join recipe_detail rd on recipe.id = rd.recipe_id
                     inner join product on rd.product_id = product.id
                     inner join product_type pt on pt.id = product.product_type
            group by product.id, pt.name, product.name
            order by pt.name, product.name""",
            nativeQuery = true)
    List<ShopListIdOnly> getShopList();
}
