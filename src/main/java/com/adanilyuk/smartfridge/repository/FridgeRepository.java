package com.adanilyuk.smartfridge.repository;

import com.adanilyuk.smartfridge.entity.Fridge;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FridgeRepository extends JpaRepository<Fridge,Integer> {
}
