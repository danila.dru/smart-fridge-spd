package com.adanilyuk.smartfridge.repository;

import com.adanilyuk.smartfridge.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product,Integer> {
}
