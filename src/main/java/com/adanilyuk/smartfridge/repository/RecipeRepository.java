package com.adanilyuk.smartfridge.repository;

import com.adanilyuk.smartfridge.entity.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecipeRepository extends JpaRepository<Recipe,Integer> {
}
