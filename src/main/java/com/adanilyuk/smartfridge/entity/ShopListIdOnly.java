package com.adanilyuk.smartfridge.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ShopListIdOnly {
    @Id
    @Column(name = "product_id")
    private Integer idProduct;
    @Column(name = "need")
    private Double need;

    public Integer getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public Double getNeed() {
        return need;
    }

    public void setNeed(Double need) {
        this.need = need;
    }
}
