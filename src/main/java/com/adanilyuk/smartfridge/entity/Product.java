package com.adanilyuk.smartfridge.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    @Size(min = 2, max = 150, message = "Name should be between 2 and 150 characters")
    private String name;

    @ManyToOne
    @JoinColumn(name = "product_type", referencedColumnName = "id")
    @NotNull(message = "Product type should not be empty")
    private ProductType productType;

    @Column(name = "measure")
    @Enumerated(EnumType.STRING)
    @NotNull(message = "Measure should not be empty")
    private Measures measure;

    @Column(name = "price")
    @NotNull(message = "Price should not be empty")
    private BigDecimal price;

    public Product() {
    }

    public Product(int id, String name, ProductType productType, Measures measure, BigDecimal price) {
        this.id = id;
        this.name = name;
        this.productType = productType;
        this.measure = measure;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public Measures getMeasure() {
        return measure;
    }

    public void setMeasure(Measures measure) {
        this.measure = measure;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
