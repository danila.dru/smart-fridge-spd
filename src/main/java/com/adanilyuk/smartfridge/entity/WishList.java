package com.adanilyuk.smartfridge.entity;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "wishlist")
public class WishList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "recipe_id", referencedColumnName = "id")
    @NotNull(message ="Recipe must be fill")
    private Recipe recipe;

    @Column(name = "amount")
    @Min(value = 1, message = "Amount mast be fill")
    private Integer amount;

    public WishList() {
    }

    public WishList(int id, Recipe recipe, Integer amount) {
        this.id = id;
        this.recipe = recipe;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
