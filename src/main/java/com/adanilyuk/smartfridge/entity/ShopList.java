package com.adanilyuk.smartfridge.entity;

import java.math.BigDecimal;

public class ShopList {
    private Product product;
    private Double need;
    private BigDecimal price;

    public ShopList() {
    }

    public ShopList(Product product, Double need, BigDecimal price) {
        this.product = product;
        this.need = need;
        this.price = price;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Double getNeed() {
        return need;
    }

    public void setNeed(Double need) {
        this.need = need;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getProductType() {
        return product.getProductType().getName();
    }

    public String getProductName() {
        return product.getName();
    }
}
