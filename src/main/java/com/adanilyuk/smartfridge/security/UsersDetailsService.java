package com.adanilyuk.smartfridge.security;

import com.adanilyuk.smartfridge.entity.Users;
import com.adanilyuk.smartfridge.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class UsersDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {
    private final UserRepository peopleRepository;

    public UsersDetailsService(UserRepository peopleRepository) {
        this.peopleRepository = peopleRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Users> person = peopleRepository.findByUsername(username);
        if (person.isEmpty()) {
            throw new UsernameNotFoundException("User not found");
        }

        return new PersonDetails(person.get());
    }
}
