package com.adanilyuk.smartfridge.controller.rest;

import com.adanilyuk.smartfridge.dto.ShopListDto;
import com.adanilyuk.smartfridge.entity.ShopList;
import com.adanilyuk.smartfridge.service.ShopListService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/shoplist")
public class RestShopListController {

    private final ShopListService shopListService;

    public RestShopListController(ShopListService shopListService) {
        this.shopListService = shopListService;
    }

    @GetMapping("")
    public List<ShopListDto> getALL() {
        return shopListService.getShopListSql().stream().map(this::convertToShopListDTO).collect(Collectors.toList());
    }

    private ShopListDto convertToShopListDTO(ShopList shopList) {
        return new ShopListDto(shopList.getProduct().getName() + " " + shopList.getProduct().getMeasure(),shopList.getNeed(),shopList.getPrice());
    }
}
