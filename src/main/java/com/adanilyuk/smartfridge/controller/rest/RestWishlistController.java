package com.adanilyuk.smartfridge.controller.rest;

import com.adanilyuk.smartfridge.dto.WishListDto;
import com.adanilyuk.smartfridge.entity.WishList;
import com.adanilyuk.smartfridge.service.WishlistService;
import org.modelmapper.ModelMapper;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/wishlist")
public class RestWishlistController {
    private final WishlistService wishlistService;
    private final ModelMapper modelMapper;

    public RestWishlistController(WishlistService wishlistService, ModelMapper modelMapper) {
        this.wishlistService = wishlistService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("")
    public List<WishListDto> getALL() {
        return wishlistService.findAll().stream().map(this::convertToWishListDTO).toList();
    }

    @PostMapping("")
    public WishListDto createWishList(@RequestBody @Valid WishListDto wishListDTO, BindingResult bindingResult) {
        WishList wishList = convertToWishList(wishListDTO);
        wishlistService.save(wishList);
        return convertToWishListDTO(wishList);
    }

    @DeleteMapping("/{id}")
    public void deleteWishList(@PathVariable("id") int wishListId) {
        wishlistService.delete(wishListId);
    }

    private WishList convertToWishList(WishListDto wishListDTO) {
        return modelMapper.map(wishListDTO, WishList.class);
    }

    private WishListDto convertToWishListDTO(WishList wishList) {
        return modelMapper.map(wishList, WishListDto.class);
    }
}