package com.adanilyuk.smartfridge.controller.http;

import com.adanilyuk.smartfridge.entity.Users;
import com.adanilyuk.smartfridge.service.UsersService;
import com.adanilyuk.smartfridge.validation.UserValidator;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/auth")
public class AuthController {
    private final UserValidator userValidator;
    private final UsersService usersService;

    public AuthController(UserValidator userValidator, UsersService usersService) {
        this.userValidator = userValidator;
        this.usersService = usersService;
    }

    @GetMapping("/login")
    public String loginPage() {
        return "auth/login";
    }

    @GetMapping("/registration")
    public String registrationPage(@ModelAttribute("person") Users user) {
        return "auth/registration";
    }

    @PostMapping("/registration")
    public String perforRegistration(@ModelAttribute("person") @Valid Users user, BindingResult bindingResult) {
        userValidator.validate(user, bindingResult);
        if (bindingResult.hasErrors()) {
            return "auth/registration";
        }
        usersService.register(user);
        return "redirect:/auth/login";
    }
}
