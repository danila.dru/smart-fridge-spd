package com.adanilyuk.smartfridge.controller.http;

import com.adanilyuk.smartfridge.entity.Fridge;
import com.adanilyuk.smartfridge.service.FridgeService;
import com.adanilyuk.smartfridge.service.ProductService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/fridge")
public class FridgeController {
    private final FridgeService fridgeService;
    private final ProductService productService;

    public FridgeController(FridgeService fridgeService, ProductService productService) {
        this.fridgeService = fridgeService;
        this.productService = productService;
    }

    @GetMapping()
    public String index(@RequestParam(value = "page", required = false, defaultValue = "0") int pageNumber,
                        @RequestParam(value = "sortField", required = false, defaultValue = "id") String sortField,
                        @RequestParam(value = "sortDir", required = false, defaultValue = "asc") String sortDir,
                        Model model) {
        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
        model.addAttribute("fridge", fridgeService.getPage(pageNumber, 3,sortField,sortDir));
        return "fridge/index";
    }

    @GetMapping("/{id}")
    public String edit(Model model, @PathVariable("id") int id) {
        Fridge foundFringe = fridgeService.show(id);
        if (foundFringe != null) {
            model.addAttribute("fridge", fridgeService.show(id));
            model.addAttribute("product", productService.findAll());
            return "fridge/edit";
        } else {
            return index(0,"id","asc",model);
        }
    }

    @PatchMapping("/{id}")
    public String update(Model model, @ModelAttribute("fridge") @Valid Fridge fridge, BindingResult bindingResult, @PathVariable("id") int id) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("product", productService.findAll());
            return "fridge/edit";
        }

        fridgeService.save(fridge);
        return "redirect:/fridge";
    }

    @GetMapping("/new")
    public String newFridge(Model model, @ModelAttribute("fridge") Fridge fridge) {
        model.addAttribute("product", productService.findAll());
        return "fridge/edit";
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") int id) {
        fridgeService.delete(id);
        return "redirect:/fridge";
    }
}
