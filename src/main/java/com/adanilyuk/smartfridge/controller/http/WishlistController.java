package com.adanilyuk.smartfridge.controller.http;

import com.adanilyuk.smartfridge.entity.WishList;
import com.adanilyuk.smartfridge.service.RecipeService;
import com.adanilyuk.smartfridge.service.WishlistService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/wishlist")
public class WishlistController {
    private final WishlistService wishlistService;
    private final RecipeService recipeService;


    public WishlistController(WishlistService wishlistService, RecipeService recipeService) {
        this.wishlistService = wishlistService;
        this.recipeService = recipeService;
    }

    @GetMapping()
    public String index(@RequestParam(value = "page", required = false, defaultValue = "0") int pageNumber,
                        @RequestParam(value = "sortField", required = false, defaultValue = "id") String sortField,
                        @RequestParam(value = "sortDir", required = false, defaultValue = "asc") String sortDir,
                        Model model) {
        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
        model.addAttribute("wishlist", wishlistService.getPage(pageNumber, 3,sortField,sortDir));
        return "wishlist/index";
    }

    @GetMapping("/{id}")
    public String edit(Model model, @PathVariable("id") int id) {
        model.addAttribute("wishlist", wishlistService.show(id));
        model.addAttribute("recipe", recipeService.findAll());
        return "wishlist/edit";
    }

    @PatchMapping("/{id}")
    public String update(Model model, @ModelAttribute("wishlist") @Valid WishList wishList, BindingResult bindingResult, @PathVariable("id") int id) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("recipe", recipeService.findAll());
            return "wishlist/edit";
        }

        wishlistService.save(wishList);
        return "redirect:/wishlist";
    }

    @GetMapping("/new")
    public String newBook(Model model, @ModelAttribute("wishlist") WishList wishList) {
        model.addAttribute("recipe", recipeService.findAll());
        return "wishlist/edit";
    }
    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") int id) {
        wishlistService.delete(id);
        return "redirect:/wishlist";
    }
}
