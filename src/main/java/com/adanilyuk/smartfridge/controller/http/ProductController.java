package com.adanilyuk.smartfridge.controller.http;

import com.adanilyuk.smartfridge.entity.Product;
import com.adanilyuk.smartfridge.service.ProductService;
import com.adanilyuk.smartfridge.service.ProductTypeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/product")
public class ProductController {
    private final ProductService productService;
    private final ProductTypeService productTypeService;

    public ProductController(ProductService productService, ProductTypeService productTypeService) {
        this.productService = productService;
        this.productTypeService = productTypeService;
    }

    @GetMapping()
    public String index(@RequestParam(value = "page", required = false, defaultValue = "0") int pageNumber,
                        @RequestParam(value = "sortField", required = false, defaultValue = "id") String sortField,
                        @RequestParam(value = "sortDir", required = false, defaultValue = "asc") String sortDir,
                        Model model) {
        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
        model.addAttribute("product", productService.getPage(pageNumber, 3,sortField,sortDir));
        return "product/index";
    }

    @GetMapping("/{id}")
    public String edit(Model model, @PathVariable("id") int id) {
        model.addAttribute("product", productService.show(id));
        model.addAttribute("productType", productTypeService.findAll());
        return "product/edit";
    }

    @PatchMapping("/{id}")
    public String update(Model model, @ModelAttribute("product") @Valid Product product, BindingResult bindingResult, @PathVariable("id") int id) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("productType", productTypeService.findAll());
            return "product/edit";
        }

        productService.save(product);
        return "redirect:/product";
    }
    @GetMapping("/new")
    public String newProduct(Model model, @ModelAttribute("product") Product product) {
        model.addAttribute("productType", productTypeService.findAll());
        return "product/edit";
    }
    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") int id) {
        productService.delete(id);
        return "redirect:/product";
    }
}
