package com.adanilyuk.smartfridge.controller.http;

import com.adanilyuk.smartfridge.entity.ShopList;
import com.adanilyuk.smartfridge.service.ShopListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;
import java.util.List;

@Controller
@RequestMapping()
public class IndexController {
    private final ShopListService shopListService;

    @Autowired
    public IndexController(ShopListService shopListService) {
        this.shopListService = shopListService;
    }
    @GetMapping()
    public String index(Model model) {
        List<ShopList> shopLists = shopListService.getShopList();
        model.addAttribute("shoplist", shopLists);
        model.addAttribute("totalsum", shopLists.stream().map(ShopList::getPrice).reduce(BigDecimal.valueOf(0.0), BigDecimal::add));
        return "index";
    }
}
