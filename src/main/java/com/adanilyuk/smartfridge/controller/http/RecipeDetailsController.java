package com.adanilyuk.smartfridge.controller.http;

import com.adanilyuk.smartfridge.entity.RecipeDetail;
import com.adanilyuk.smartfridge.entity.Recipe;
import com.adanilyuk.smartfridge.service.ProductService;
import com.adanilyuk.smartfridge.service.RecipeDetailsService;
import com.adanilyuk.smartfridge.service.RecipeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/recipedetails")
public class RecipeDetailsController {
    private final RecipeDetailsService recipeDetailsService;
    private final RecipeService recipeService;
    private final ProductService productService;


    public RecipeDetailsController(RecipeDetailsService recipeDetailsService, RecipeService recipeService, ProductService productService) {
        this.recipeDetailsService = recipeDetailsService;
        this.recipeService = recipeService;
        this.productService = productService;
    }

    @GetMapping()
    public String index(Model model) {
        model.addAttribute("recipedetails", recipeDetailsService.findAll());
        return "recipedetails/index";
    }

    @GetMapping("/{id}")
    public String edit(Model model, @PathVariable("id") int id) {
        RecipeDetail recipeDetails = recipeDetailsService.show(id);
        if (recipeDetails != null) {
            model.addAttribute("recipedetails", recipeDetails);
            model.addAttribute("recipe", recipeService.findAll());
            model.addAttribute("product", productService.findAll());
            return "recipedetails/edit";
        } else {
            return index(model);
        }
    }

    @PatchMapping("/{id}")
    public String update(Model model, @ModelAttribute("recipedetails") @Valid RecipeDetail recipeDetails, BindingResult bindingResult, @PathVariable("id") int id) {
        if (bindingResult.hasErrors()) {
            return "recipedetails/edit";
        }
        recipeDetailsService.save(recipeDetails);
        return "redirect:/recipe/" + recipeDetails.getRecipe().getId();
    }

    @GetMapping("/new")
    public String newRecipeDetail(Model model, @ModelAttribute("recipedetails") RecipeDetail recipeDetail) {
        model.addAttribute("recipe", recipeService.findAll());
        model.addAttribute("product", productService.findAll());
        return "recipedetails/edit";
    }

    @GetMapping("/new/{idrecipe}")
    public String newRecipeDetail(Model model, @ModelAttribute("recipedetails") RecipeDetail recipeDetail, @PathVariable("idrecipe") int idrecipe) {
        Recipe recipe = recipeService.show(idrecipe);
        model.addAttribute("recipedetails", new RecipeDetail(recipe));
        model.addAttribute("recipe", recipeService.findAll());
        model.addAttribute("product", productService.findAll());
        return "recipedetails/edit";
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") int id) {
        RecipeDetail recipeDetails = recipeDetailsService.show(id);
        if (recipeDetails != null) {
            int idRecipe = recipeDetails.getRecipe().getId();
            recipeDetailsService.delete(id);
            return "redirect:/recipe/" + idRecipe;
        } else {
            return "redirect:/recipedetails";
        }
    }
}
