package com.adanilyuk.smartfridge.controller.http;

import com.adanilyuk.smartfridge.entity.Recipe;
import com.adanilyuk.smartfridge.service.RecipeDetailsService;
import com.adanilyuk.smartfridge.service.RecipeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/recipe")
public class RecipeController {
    private final RecipeService recipeService;
    private final RecipeDetailsService recipeDetailsService;

    public RecipeController(RecipeService recipeService, RecipeDetailsService recipeDetailsService) {
        this.recipeService = recipeService;
        this.recipeDetailsService = recipeDetailsService;
    }

    @GetMapping()
    public String index(@RequestParam(value = "page", required = false, defaultValue = "0") int pageNumber,
                        @RequestParam(value = "sortField", required = false, defaultValue = "id") String sortField,
                        @RequestParam(value = "sortDir", required = false, defaultValue = "asc") String sortDir,
                        Model model) {
        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
        model.addAttribute("recipe", recipeService.getPage(pageNumber, 3,sortField,sortDir));
        return "recipe/index";
    }

    @GetMapping("/{id}")
    public String edit(Model model, @PathVariable("id") int id) {
        Recipe recipe = recipeService.show(id);
        if (recipe != null) {
            model.addAttribute("recipe",recipe);
            model.addAttribute("recipeDetails", recipeDetailsService.findByRecipe(recipe));
            return "recipe/edit";
        } else {
            return index(0,"id","asc",model);
        }
    }

    @PatchMapping("/{id}")
    public String update(Model model, @ModelAttribute("recipe") @Valid Recipe recipe, BindingResult bindingResult, @PathVariable("id") int id) {
        if (bindingResult.hasErrors()) {
            return "recipe/edit";
        }

        recipeService.save(recipe);
        return "redirect:/recipe";
    }

    @GetMapping("/new")
    public String newRecipe(Model model, @ModelAttribute("recipe") Recipe recipe) {
        return "recipe/edit";
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") int id) {
        recipeService.delete(id);
        return "redirect:/recipe";
    }
}
