package com.adanilyuk.smartfridge.dto;

import javax.validation.constraints.Min;

public class WishListDto {
    private int id;
    private int idRecipe;
    @Min(value = 1, message = "Amount mast be fill")
    private Integer amount;

    public int getIdRecipe() {
        return idRecipe;
    }

    public void setIdRecipe(int idRecipe) {
        this.idRecipe = idRecipe;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
