package com.adanilyuk.smartfridge.dto;

import java.math.BigDecimal;

public class ShopListDto {
    private String product;
    private Double need;
    private BigDecimal price;

    public ShopListDto(String product, Double need, BigDecimal price) {
        this.product = product;
        this.need = need;
        this.price = price;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Double getNeed() {
        return need;
    }

    public void setNeed(Double need) {
        this.need = need;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
