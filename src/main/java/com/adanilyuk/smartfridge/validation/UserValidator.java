package com.adanilyuk.smartfridge.validation;

import com.adanilyuk.smartfridge.entity.Users;
import com.adanilyuk.smartfridge.service.UsersService;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {
    private final UsersService usersService;

    public UserValidator(UsersService usersService) {
        this.usersService = usersService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return Users.class.equals(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Users currentUser = (Users) target;
        if (usersService.usernameIsExist(currentUser.getUsername())){
            errors.rejectValue("username","name already exist");
        }
    }
}
