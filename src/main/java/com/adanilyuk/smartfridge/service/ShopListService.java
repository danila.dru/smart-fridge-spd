package com.adanilyuk.smartfridge.service;

import com.adanilyuk.smartfridge.entity.*;
import com.adanilyuk.smartfridge.repository.ShopListRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class ShopListService {

    private final ShopListRepository shopListRepository;
    private final ProductService productService;

    private final WishlistService wishlistService;
    private final RecipeDetailsService recipeDetailsService;
    private final FridgeService fridgeService;


    public ShopListService(ShopListRepository shopListRepository, ProductService productService, WishlistService wishlistService, RecipeDetailsService recipeDetailsService, FridgeService fridgeService) {
        this.shopListRepository = shopListRepository;
        this.productService = productService;
        this.wishlistService = wishlistService;
        this.recipeDetailsService = recipeDetailsService;
        this.fridgeService = fridgeService;
    }

    public List<ShopList> getShopListSql() {
        List<ShopListIdOnly> shopListIdOnlies = shopListRepository.getShopList();

        List<ShopList> shopLists = new ArrayList<>();
        for (ShopListIdOnly shopListIdOnly :
                shopListIdOnlies) {
            if (shopListIdOnly.getNeed() <= 0) {
                continue;
            }
            Product product = productService.show(shopListIdOnly.getIdProduct());
            shopLists.add(new ShopList(product,
                    shopListIdOnly.getNeed(),
                    product.getPrice().multiply(BigDecimal.valueOf(shopListIdOnly.getNeed()))));
        }
        return shopLists;
    }

    public static void updateValue(Map<Integer, Float> map, Integer key, Float value) {
        if (map.containsKey(key)) {
            map.put(key, map.get(key) + value);
        } else {
            map.put(key, value);
        }
    }

    private Float getDifference(Float recipeAmount, Float fridgeAmount) {
        return Objects.nonNull(fridgeAmount)
                ? recipeAmount - fridgeAmount
                : recipeAmount;
    }

    public List<ShopList> getShopList() {
        Map<Integer, Float> needMap = new HashMap<>();
        for (WishList wishList :
                wishlistService.findAll()) {
            for (RecipeDetail recipeDetail :
                    recipeDetailsService.findByRecipe(wishList.getRecipe())) {
                updateValue(needMap, recipeDetail.getProduct().getId(), wishList.getAmount() * recipeDetail.getAmount());
            }
        }

        Map<Integer, Float> freedgeMap = new HashMap<>();
        for (Fridge fridge :
                fridgeService.findAll()) {
            updateValue(freedgeMap, fridge.getProduct().getId(), fridge.getAmount());
        }

        List<ShopList> shopLists = new ArrayList<>();
        for (Map.Entry<Integer, Float> entry : needMap.entrySet()) {
            Float needAmount = getDifference(entry.getValue(), freedgeMap.get(entry.getKey()));
            if (needAmount <= 0) {
                continue;
            }
            ShopList shopList = new ShopList();
            shopList.setProduct(productService.show(entry.getKey()));
            shopList.setNeed(Double.valueOf(needAmount));
            shopList.setPrice(productService.show(entry.getKey()).getPrice().multiply(BigDecimal.valueOf(needAmount)));
            shopLists.add(shopList);
        }

        shopLists.sort(Comparator.comparing(ShopList::getProductType).thenComparing(ShopList::getProductName));
        return shopLists;
    }

}
