package com.adanilyuk.smartfridge.service;

import com.adanilyuk.smartfridge.entity.Product;
import com.adanilyuk.smartfridge.repository.ProductRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class ProductService {
    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> findAll(){
        return productRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
    }

    public Page<Product> getPage(int pageNumber, int size, String sortField, String sortDirection){
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :
                Sort.by(sortField).descending();
        PageRequest pageRequest = PageRequest.of(pageNumber,size,sort);
        return productRepository.findAll(pageRequest);
    }

    public Product show(int id) {
        Optional<Product> foundProduct  = productRepository.findById(id);
        return foundProduct.orElse(null);
    }

    @Transactional
    public void save(Product product) {
        productRepository.save(product);
    }

    @Transactional
    public void delete(int id) {
        productRepository.deleteById(id);
    }
}
