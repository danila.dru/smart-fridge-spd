package com.adanilyuk.smartfridge.service;

import com.adanilyuk.smartfridge.entity.WishList;
import com.adanilyuk.smartfridge.repository.WishlistRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class WishlistService {
    private  final WishlistRepository wishlistRepository;

    public WishlistService(WishlistRepository wishlistRepository) {
        this.wishlistRepository = wishlistRepository;
    }

    public List<WishList> findAll(){
        return wishlistRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
    }

    public Page<WishList> getPage(int pageNumber, int size, String sortField, String sortDirection){
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :
                Sort.by(sortField).descending();
        PageRequest pageRequest = PageRequest.of(pageNumber,size,sort);
        return wishlistRepository.findAll(pageRequest);
    }

    public WishList show(int id) {
        Optional<WishList> foundWishlist  = wishlistRepository.findById(id);
        return foundWishlist.orElse(null);
    }
    @Transactional
    public void save(WishList wishList) {
        wishlistRepository.save(wishList);
    }

    @Transactional
    public void delete(int id) {
        wishlistRepository.deleteById(id);
    }
}
