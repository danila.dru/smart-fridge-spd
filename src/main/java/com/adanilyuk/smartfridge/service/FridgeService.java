package com.adanilyuk.smartfridge.service;

import com.adanilyuk.smartfridge.entity.Fridge;
import com.adanilyuk.smartfridge.repository.FridgeRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class FridgeService {
    private final FridgeRepository fridgeRepository;

    public FridgeService(FridgeRepository fridgeRepository) {
        this.fridgeRepository = fridgeRepository;
    }

    public List<Fridge> findAll() {
        return fridgeRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
    }

    public Page<Fridge> getPage(int pageNumber, int size, String sortField, String sortDirection) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :
                Sort.by(sortField).descending();
        PageRequest pageRequest = PageRequest.of(pageNumber, size, sort);
        return fridgeRepository.findAll(pageRequest);
    }

    public Fridge show(int id) {
        Optional<Fridge> foundFridge = fridgeRepository.findById(id);
        return foundFridge.orElse(null);
    }

    @Transactional
    public void save(Fridge fridge) {
        fridgeRepository.save(fridge);
    }

    @Transactional
    public void delete(int id) {
        fridgeRepository.deleteById(id);
    }

    @Transactional
    public void deleteAll() {
        fridgeRepository.deleteAll();
    }
}
