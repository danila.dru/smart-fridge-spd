package com.adanilyuk.smartfridge.service;

import com.adanilyuk.smartfridge.entity.ProductType;
import com.adanilyuk.smartfridge.repository.ProductTypeRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class ProductTypeService {
    private final ProductTypeRepository productTypeRepository;

    public ProductTypeService(ProductTypeRepository productTypeRepository) {
        this.productTypeRepository = productTypeRepository;
    }

    public List<ProductType> findAll(){
        return productTypeRepository.findAll();
    }

    @Transactional
    public void save(ProductType productType) {
        productTypeRepository.save(productType);
    }

    public ProductType show(int id) {
        Optional<ProductType> foundProductType = productTypeRepository.findById(id);
        return foundProductType.orElse(null);
    }
}
