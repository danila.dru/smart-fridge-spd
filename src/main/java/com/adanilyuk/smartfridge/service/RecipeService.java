package com.adanilyuk.smartfridge.service;

import com.adanilyuk.smartfridge.entity.Recipe;
import com.adanilyuk.smartfridge.repository.RecipeRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class RecipeService {
    private final RecipeRepository recipeRepository;

    public RecipeService(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    public List<Recipe> findAll(){
        return recipeRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
    }

    public Page<Recipe> getPage(int pageNumber, int size, String sortField, String sortDirection){
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :
                Sort.by(sortField).descending();
        PageRequest pageRequest = PageRequest.of(pageNumber,size,sort);
        return recipeRepository.findAll(pageRequest);
    }

    public Recipe show(int id) {
        Optional<Recipe> foundRecipe  = recipeRepository.findById(id);
        return foundRecipe.orElse(null);
    }

    @Transactional
    public void save(Recipe recipe) {
        recipeRepository.save(recipe);
    }

    @Transactional
    public void delete(int id) {
        recipeRepository.deleteById(id);
    }
}
