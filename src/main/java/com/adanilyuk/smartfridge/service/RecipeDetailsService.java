
package com.adanilyuk.smartfridge.service;

import com.adanilyuk.smartfridge.entity.RecipeDetail;
import com.adanilyuk.smartfridge.entity.Recipe;
import com.adanilyuk.smartfridge.repository.RecipeDetailsRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class RecipeDetailsService {
    private final RecipeDetailsRepository recipeDetailsRepository;

    public RecipeDetailsService(RecipeDetailsRepository recipeDetailsRepository) {
        this.recipeDetailsRepository = recipeDetailsRepository;
    }
    public List<RecipeDetail> findAll(){
        return recipeDetailsRepository.findAll(Sort.by(Sort.Direction.ASC, "recipe"));
    }

    public List<RecipeDetail> findByRecipe(Recipe recipe){
        return recipeDetailsRepository.findAllByRecipeOrderById(recipe);
    }

    public RecipeDetail show(int id) {
        Optional<RecipeDetail> foundRecipeDetails  = recipeDetailsRepository.findById(id);
        return foundRecipeDetails.orElse(null);
    }

    @Transactional
    public void save(RecipeDetail recipeDetails) {
        recipeDetailsRepository.save(recipeDetails);
    }

    @Transactional
    public void delete(int id) {
        recipeDetailsRepository.deleteById(id);
    }
}
