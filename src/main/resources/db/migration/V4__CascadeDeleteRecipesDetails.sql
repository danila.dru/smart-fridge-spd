alter table recipe_detail
    drop constraint recipe_detail_recipe_id_fkey;

alter table recipe_detail
    add foreign key (recipe_id) references recipe
        on delete cascade;