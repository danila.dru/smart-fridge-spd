/*add types*/
insert into product_type(id, name)
values (1, 'Овочі');
insert into product_type(id, name)
values (2, 'Фрукти');
insert into product_type(id, name)
values (3, 'Каші');
insert into product_type(id, name)
values (4, 'Молочна продукція');
insert into product_type(id, name)
values (5, 'Бакалія');
insert into product_type(id, name)
values (6, 'Соуси');
insert into product_type(id, name)
values (7, 'Мучні продукти');
insert into product_type(id, name)
values (8, 'Спеції');
insert into product_type(id, name)
values (9, 'М’ясо');
insert into product_type(id, name)
values (10, 'Риба');

alter sequence product_type_id_seq restart with 11;

/*add products*/
insert into product(id, name, product_type, measure, price)
values (1, 'Помідори', 1, 'KG', 65);
insert into product(id, name, product_type, measure, price)
values (2, 'Огірки', 1, 'KG', 70);
insert into product(id, name, product_type, measure, price)
values (3, 'Кабачок', 1, 'KG', 50);

insert into product(id, name, product_type, measure, price)
values (4, 'Яблуко', 2, 'KG', 25);
insert into product(id, name, product_type, measure, price)
values (5, 'Вишня', 2, 'KG', 80);
insert into product(id, name, product_type, measure, price)
values (6, 'Полуниця', 2, 'KG', 60);

insert into product(id, name, product_type, measure, price)
values (7, 'Гречка', 3, 'KG', 80);
insert into product(id, name, product_type, measure, price)
values (8, 'Вівсянка', 3, 'KG', 35);
insert into product(id, name, product_type, measure, price)
values (9, 'Рис', 3, 'KG', 70);

insert into product(id, name, product_type, measure, price)
values (10, 'Молоко', 4, 'LITER', 35);
insert into product(id, name, product_type, measure, price)
values (11, 'Сметана', 4, 'LITER', 120);

insert into product(id, name, product_type, measure, price)
values (12, 'Борошно', 7, 'KG', 40);
insert into product(id, name, product_type, measure, price)
values (13, 'Дріжджі', 7, 'KG', 200);

insert into product(id, name, product_type, measure, price)
values (14, 'Свинина биток', 9, 'KG', 145);
insert into product(id, name, product_type, measure, price)
values (15, 'Курка філе', 9, 'KG', 100);

insert into product(id, name, product_type, measure, price)
values (16, 'Хек морожений', 10, 'KG', 120);

insert into product(id, name, product_type, measure, price)
values (17, 'олія', 5, 'LITER', 80);

alter sequence product_id_seq restart with 20;

/*add recipes*/
insert into recipe(id, name)
values (1, 'Салат з огірків та помідорів');
insert into recipe(id, name)
values (2, 'Вареники з вишнями');
insert into recipe(id, name)
values (3, 'Вареники з полуницею');
insert into recipe(id, name)
values (4, 'Пиріжки з яблуками');

alter sequence recipe_id_seq restart with 5;

/*add recipes 'Салат з огірків та помідорів' */
insert into recipe_detail(id, recipe_id, product_id, amount)
values (1, 1, 1, 0.5);
insert into recipe_detail(id, recipe_id, product_id, amount)
values (2, 1, 2, 0.5);
insert into recipe_detail(id, recipe_id, product_id, amount)
values (3, 1, 17, 0.15);


/*add recipes 'Вареники з вишнями' */
insert into recipe_detail(id, recipe_id, product_id, amount)
values (4, 2, 12, 0.5);
insert into recipe_detail(id, recipe_id, product_id, amount)
values (5, 2, 5, 1);
insert into recipe_detail(id, recipe_id, product_id, amount)
values (6, 2, 13, 0.1);


/*add recipes 'Вареники з полуницею' */
insert into recipe_detail(id, recipe_id, product_id, amount)
values (7, 3, 12, 0.5);
insert into recipe_detail(id, recipe_id, product_id, amount)
values (8, 3, 6, 1);
insert into recipe_detail(id, recipe_id, product_id, amount)
values (9, 3, 13, 0.1);

/*add recipes 'Пиріжки з яблуками' */
insert into recipe_detail(id, recipe_id, product_id, amount)
values (10, 4, 12, 0.5);
insert into recipe_detail(id, recipe_id, product_id, amount)
values (11, 4, 4, 1);
insert into recipe_detail(id, recipe_id, product_id, amount)
values (12, 4, 13, 0.1);

alter sequence recipe_detail_id_seq restart with 15;

/*add fridge status*/
insert into fridge(id, product_id, amount)
values (1, 1, 0.2);
insert into fridge(id, product_id, amount)
values (2, 2, 0.5);
insert into fridge(id, product_id, amount)
values (3, 3, 0.7);
insert into fridge(id, product_id, amount)
values (4, 2, 0.2);
alter sequence fridge_id_seq restart with 5;


/*add wishlist*/
insert into wishlist(id, recipe_id, amount)
values (1, 1, 1);
insert into wishlist(id, recipe_id, amount)
values (2, 2, 2);
insert into wishlist(id, recipe_id, amount)
values (3, 3, 1);
insert into wishlist(id, recipe_id, amount)
values (4, 1, 2);
alter sequence wishlist_id_seq restart with 5;