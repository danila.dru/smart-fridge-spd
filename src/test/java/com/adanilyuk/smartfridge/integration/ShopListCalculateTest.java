package com.adanilyuk.smartfridge.integration;

import com.adanilyuk.smartfridge.entity.*;
import com.adanilyuk.smartfridge.service.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

class ShopListCalculateTest extends BaseIntegrationTest {
    @Autowired
    ProductTypeService productTypeService;

    @Autowired
    ProductService productService;

    @Autowired
    RecipeService recipeService;

    @Autowired
    RecipeDetailsService recipeDetailsService;

    @Autowired
    WishlistService wishlistService;

    @Autowired
    ShopListService shopListService;

    @Autowired
    FridgeService fridgeService;

    private Product product1;
    private Product product2;

    private Recipe recipe;

    @BeforeEach
    private void prepareData() {
        ProductType productType = new ProductType(1, "Vegetables");
        productTypeService.save(productType);

        product1 = new Product(1, "Potatoes", productType, Measures.KG, BigDecimal.valueOf(50));
        productService.save(product1);

        product2 = new Product(2, "Cucumber", productType, Measures.KG, BigDecimal.valueOf(60));
        productService.save(product2);

        recipe = new Recipe(1, "Test recipe");
        recipeService.save(recipe);

        RecipeDetail recipeDetail1 = new RecipeDetail(1, recipe, product1, 1f);
        recipeDetailsService.save(recipeDetail1);

        RecipeDetail recipeDetail2 = new RecipeDetail(2, recipe, product2, 2f);
        recipeDetailsService.save(recipeDetail2);

        WishList wishList = new WishList(1, recipe, 10);
        wishlistService.save(wishList);
    }

    @Test
    void testAddData() {
        Assertions.assertNotNull(productTypeService.show(1));
        Assertions.assertEquals("Vegetables", productTypeService.show(1).getName());


        Assertions.assertNotNull(productService.show(1));
        Assertions.assertEquals("Potatoes", productService.show(1).getName());
        Assertions.assertEquals(productService.show(1).getPrice(), BigDecimal.valueOf(50));

        Assertions.assertNotNull(productService.show(2));
        Assertions.assertEquals("Cucumber", productService.show(2).getName());
        Assertions.assertEquals(productService.show(2).getPrice(), BigDecimal.valueOf(60));


        Assertions.assertNotNull(recipeService.show(1));
        Assertions.assertEquals("Test recipe", recipeService.show(1).getName());


        Assertions.assertNotNull(recipeDetailsService.show(1));
        Assertions.assertEquals(recipeDetailsService.show(1).getRecipe().getId(), recipe.getId());
        Assertions.assertEquals(recipeDetailsService.show(1).getProduct().getId(), product1.getId());

        Assertions.assertNotNull(recipeDetailsService.show(2));
        Assertions.assertEquals(recipeDetailsService.show(2).getRecipe().getId(), recipe.getId());
        Assertions.assertEquals(recipeDetailsService.show(2).getProduct().getId(), product2.getId());

        Assertions.assertNotNull(wishlistService.show(1));
        Assertions.assertEquals(wishlistService.show(1).getAmount(), Integer.valueOf(10));
    }

    @Test
    void testShopListOnEmptyFridge() {
        fridgeService.deleteAll();

        List<ShopList> etalonShopList = new ArrayList<>();
        etalonShopList.add(new ShopList(product2, 20d, BigDecimal.valueOf(60d)));
        etalonShopList.add(new ShopList(product1, 10d, BigDecimal.valueOf(50d)));

        List<ShopList> shopList = shopListService.getShopList();
        Assertions.assertEquals(etalonShopList.size(),shopList.size());
        Assertions.assertEquals(etalonShopList.get(0).getNeed(),shopList.get(0).getNeed());
        Assertions.assertEquals(etalonShopList.get(1).getNeed(),shopList.get(1).getNeed());
    }

    @Test
    void testShopListOnNonEmptyFridge() {
        fridgeService.deleteAll();
        Fridge fridge = new Fridge(1, product1, 5f);
        fridgeService.save(fridge);

        List<ShopList> etalonShopList = new ArrayList<>();
        etalonShopList.add(new ShopList(product2, 20d, BigDecimal.valueOf(60d)));
        etalonShopList.add(new ShopList(product1, 5d, BigDecimal.valueOf(50d)));

        List<ShopList> shopList = shopListService.getShopList();
        Assertions.assertEquals(etalonShopList.size(),shopList.size());
        Assertions.assertEquals(etalonShopList.get(0).getNeed(),shopList.get(0).getNeed());
        Assertions.assertEquals(etalonShopList.get(1).getNeed(),shopList.get(1).getNeed());
    }

    @Test
    void testShopListOnFullFridge() {
        fridgeService.deleteAll();
        fridgeService.save(new Fridge(1, product1, 50f));
        fridgeService.save(new Fridge(2, product2, 50f));

        List<ShopList> shopList = shopListService.getShopList();
        Assertions.assertEquals(0, shopList.size());
    }
}
