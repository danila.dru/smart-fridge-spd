package com.adanilyuk.smartfridge.unit.controllers;

import com.adanilyuk.smartfridge.controller.http.ProductController;
import com.adanilyuk.smartfridge.entity.Measures;
import com.adanilyuk.smartfridge.entity.ProductType;
import com.adanilyuk.smartfridge.entity.Product;
import com.adanilyuk.smartfridge.security.AuthProvider;
import com.adanilyuk.smartfridge.service.ProductService;
import com.adanilyuk.smartfridge.service.ProductTypeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Collections;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ProductController.class)
class ProductControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthProvider authProvider;

    @MockBean
    private ProductService productService;

    @MockBean
    private ProductTypeService productTypeService;

    @Test
    @WithMockUser(username = "admin", password = "admin")
    void shouldReturnProductIndexPage() throws Exception {
        ProductType testProductType = new ProductType(1,"test type");
        Product testProduct  = new Product(1,"test product",testProductType, Measures.KG, BigDecimal.valueOf(100));
        when(productService.findAll()).thenReturn(Collections.singletonList(testProduct));

        this.mockMvc
                .perform(get("/product"))
                .andExpect(status().isOk())
                .andExpect(view().name("product/index"))
                .andExpect(model().attributeExists("product"));
    }

    @Test
    @WithMockUser(username = "admin", password = "admin")
    void shouldReturnProductViewPage() throws Exception {
        ProductType testProductType = new ProductType(1,"test type");
        Product testProduct  = new Product(1,"test product",testProductType, Measures.KG, BigDecimal.valueOf(100));
        when(productService.show(1)).thenReturn(testProduct);

        this.mockMvc
                .perform(get("/product/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("product/edit"))
                .andExpect(model().attributeExists("product"))
                .andExpect(model().attributeExists("productType"));
    }

    @Test
    @WithMockUser(username = "admin", password = "admin")
    void shouldReturnNewProductViewPage() throws Exception {
        ProductType testProductType = new ProductType(1,"test type");
        Product testProduct  = new Product(1,"test product",testProductType, Measures.KG, BigDecimal.valueOf(100));
        when(productService.show(1)).thenReturn(testProduct);

        this.mockMvc
                .perform(get("/product/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("product/edit"))
                .andExpect(model().attributeExists("product"))
                .andExpect(model().attributeExists("productType"));
    }

    @Test
    void shouldDeleteProduct() throws Exception {
        this.mockMvc
                .perform(delete("/product/1")
                        .with(user("admin"))
                        .with(csrf())
                );
        verify(productService).delete(1);
    }



}