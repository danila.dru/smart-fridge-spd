package com.adanilyuk.smartfridge.unit.controllers;

import com.adanilyuk.smartfridge.controller.http.WishlistController;
import com.adanilyuk.smartfridge.entity.*;
import com.adanilyuk.smartfridge.security.AuthProvider;
import com.adanilyuk.smartfridge.service.RecipeService;
import com.adanilyuk.smartfridge.service.WishlistService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(WishlistController.class)
class WishlistControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthProvider authProvider;

    @MockBean
    private WishlistService wishlistService;
    @MockBean
    private RecipeService recipeService;

    @Test
    @WithMockUser(username = "admin", password = "admin")
    void shouldReturnWishlistIndexPage() throws Exception {
        Recipe testRecipe = new Recipe(1,"test recipe");
        WishList testWishLis = new WishList(1,testRecipe,2);
        when(wishlistService.findAll()).thenReturn(Collections.singletonList(testWishLis));

        this.mockMvc
                .perform(get("/wishlist"))
                .andExpect(status().isOk())
                .andExpect(view().name("wishlist/index"))
                .andExpect(model().attributeExists("wishlist"));
    }

    @Test
    @WithMockUser(username = "admin", password = "admin")
    void shouldReturnWishlistViewPage() throws Exception {
        Recipe testRecipe = new Recipe(1,"test recipe");
        WishList testWishLis = new WishList(1,testRecipe,2);
        when(wishlistService.show(1)).thenReturn(testWishLis);

        this.mockMvc
                .perform(get("/wishlist/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("wishlist/edit"))
                .andExpect(model().attributeExists("recipe"))
                .andExpect(model().attributeExists("wishlist"));
    }

    @Test
    @WithMockUser(username = "admin", password = "admin")
    void shouldReturnNewWishlistViewPage() throws Exception {
        this.mockMvc
                .perform(get("/wishlist/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("wishlist/edit"))
                .andExpect(model().attributeExists("recipe"));
    }

    @Test
    void shouldDeleteWishlistDetails() throws Exception {
        this.mockMvc
                .perform(delete("/wishlist/1")
                        .with(user("admin"))
                        .with(csrf())
                );
        verify(wishlistService).delete(1);
    }

}