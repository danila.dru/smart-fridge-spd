package com.adanilyuk.smartfridge.unit.controllers;

import com.adanilyuk.smartfridge.controller.http.RecipeDetailsController;
import com.adanilyuk.smartfridge.entity.*;
import com.adanilyuk.smartfridge.security.AuthProvider;
import com.adanilyuk.smartfridge.service.ProductService;
import com.adanilyuk.smartfridge.service.RecipeDetailsService;
import com.adanilyuk.smartfridge.service.RecipeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Collections;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(RecipeDetailsController.class)
class RecipeDetailsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthProvider authProvider;

    @MockBean
    private RecipeDetailsService recipeDetailsService;

    @MockBean
    private RecipeService recipeService;

    @MockBean
    private ProductService productService;

    @Test
    @WithMockUser(username = "admin", password = "admin")
    void shouldReturnRecipeDetailsIndexPage() throws Exception {
        Recipe testRecipe = new Recipe(1,"test recipe");

        ProductType testProductType = new ProductType(1,"test type");
        Product testProduct  = new Product(1,"test product",testProductType, Measures.KG, BigDecimal.valueOf(100));

        RecipeDetail testRecipeDetail = new RecipeDetail(1,testRecipe,testProduct,1.1f );
        when(recipeDetailsService.findAll()).thenReturn(Collections.singletonList(testRecipeDetail));

        this.mockMvc
                .perform(get("/recipedetails"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipedetails/index"))
                .andExpect(model().attributeExists("recipedetails"));
    }

    @Test
    @WithMockUser(username = "admin", password = "admin")
    void shouldReturnRecipeDetailsViewPage() throws Exception {
        Recipe testRecipe = new Recipe(1,"test recipe");

        ProductType testProductType = new ProductType(1,"test type");
        Product testProduct  = new Product(1,"test product",testProductType, Measures.KG, BigDecimal.valueOf(100));

        RecipeDetail testRecipeDetail = new RecipeDetail(1,testRecipe,testProduct,1.1f );
        when(recipeDetailsService.show(1)).thenReturn(testRecipeDetail);

        this.mockMvc
                .perform(get("/recipedetails/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipedetails/edit"))
                .andExpect(model().attributeExists("recipe"))
                .andExpect(model().attributeExists("recipedetails"))
                .andExpect(model().attributeExists("product"));
    }

    @Test
    @WithMockUser(username = "admin", password = "admin")
    void shouldReturnNewRecipeDetailsViewPage() throws Exception {
        this.mockMvc
                .perform(get("/recipedetails/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipedetails/edit"))
                .andExpect(model().attributeExists("recipe"))
                .andExpect(model().attributeExists("product"));
    }

    @Test
    void shouldDeleteRecipeDetails() throws Exception {
        Recipe testRecipe = new Recipe(1,"test recipe");

        ProductType testProductType = new ProductType(1,"test type");
        Product testProduct  = new Product(1,"test product",testProductType, Measures.KG, BigDecimal.valueOf(100));

        RecipeDetail testRecipeDetail = new RecipeDetail(1,testRecipe,testProduct,1.1f );
        when(recipeDetailsService.show(1)).thenReturn(testRecipeDetail);

        this.mockMvc
                .perform(delete("/recipedetails/1")
                        .with(user("admin"))
                        .with(csrf())
                );
        verify(recipeDetailsService).delete(1);
    }
}