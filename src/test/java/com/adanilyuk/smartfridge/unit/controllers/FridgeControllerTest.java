package com.adanilyuk.smartfridge.unit.controllers;

import com.adanilyuk.smartfridge.controller.http.FridgeController;
import com.adanilyuk.smartfridge.entity.Fridge;
import com.adanilyuk.smartfridge.entity.Measures;
import com.adanilyuk.smartfridge.entity.ProductType;
import com.adanilyuk.smartfridge.entity.Product;
import com.adanilyuk.smartfridge.security.AuthProvider;
import com.adanilyuk.smartfridge.service.FridgeService;
import com.adanilyuk.smartfridge.service.ProductService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Collections;

import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(FridgeController.class)
class FridgeControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthProvider authProvider;

    @MockBean
    private FridgeService fridgeService;

    @MockBean
    private ProductService productService;

    @Test
    @WithMockUser(username = "admin", password = "admin")
    void shouldReturnFrigeIndexPage() throws Exception {
        ProductType testProductType = new ProductType(1,"test type");
        Product testProduct  = new Product(1,"test product",testProductType,Measures.KG, BigDecimal.valueOf(100));
        Fridge testFridge = new Fridge(1,testProduct,1.1f);
        when(fridgeService.findAll()).thenReturn(Collections.singletonList(testFridge));

        this.mockMvc
                .perform(get("/fridge"))
                .andExpect(status().isOk())
                .andExpect(view().name("fridge/index"))
                .andExpect(model().attributeExists("fridge"));
    }

    @Test
    @WithMockUser(username = "admin", password = "admin")
    void shouldReturnFrigeView() throws Exception {
        ProductType testProductType = new ProductType(1,"test type");
        Product testProduct  = new Product(1,"test product",testProductType,Measures.KG, BigDecimal.valueOf(100));
        Fridge testFridge = new Fridge(1,testProduct,1.1f);
        when(fridgeService.show(1)).thenReturn(testFridge);

        this.mockMvc
                .perform(get("/fridge/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("fridge/edit"))
                .andExpect(model().attributeExists("fridge"))
                .andExpect(model().attributeExists("product"));
    }

    @Test
    @WithMockUser(username = "admin", password = "admin")
    void shouldReturnOnWrongIdIndexPage() throws Exception {
        when(fridgeService.show(1)).thenReturn(null);
        this.mockMvc
                .perform(get("/fridge/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("fridge/index"))
                .andExpect(model().attributeExists("fridge"));
    }

    @Test
    @WithMockUser(username = "admin", password = "admin")
    void openFormForNew() throws Exception {
        this.mockMvc
                .perform(get("/fridge/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("fridge/edit"))
                .andExpect(model().attributeExists("fridge"));
    }

    @Test
    void shouldRejectDeleteFridge() throws Exception {
        this.mockMvc
                .perform(delete("/fridge/1"))
                .andExpect(status().isForbidden());
    }

    @Test
    void shouldDeleteFridge() throws Exception {
        this.mockMvc
                .perform(delete("/fridge/1")
                        .with(user("admin"))
                        .with(csrf())
                );
        verify(fridgeService).delete(1);
    }

    @Test
    void shouldCallEditFridge() throws Exception {
        this.mockMvc
                .perform(patch("/fridge/1")
                        .with(user("admin"))
                        .with(csrf())
                );
        verify(fridgeService).save(any());
   }
}