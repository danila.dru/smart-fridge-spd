package com.adanilyuk.smartfridge.unit.controllers;

import com.adanilyuk.smartfridge.controller.http.RecipeController;
import com.adanilyuk.smartfridge.entity.Recipe;
import com.adanilyuk.smartfridge.security.AuthProvider;
import com.adanilyuk.smartfridge.service.RecipeDetailsService;
import com.adanilyuk.smartfridge.service.RecipeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(RecipeController.class)
class RecipeControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthProvider authProvider;

    @MockBean
    private RecipeService recipeService;
    @MockBean
    private RecipeDetailsService recipeDetailsService;

    @Test
    @WithMockUser(username = "admin", password = "admin")
    void shouldReturnRecipeIndexPage() throws Exception {
        Recipe testRecipe = new Recipe(1,"test recipe");
        when(recipeService.findAll()).thenReturn(Collections.singletonList(testRecipe));

        this.mockMvc
                .perform(get("/recipe"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe/index"))
                .andExpect(model().attributeExists("recipe"));
    }

    @Test
    @WithMockUser(username = "admin", password = "admin")
    void shouldReturnRecipeViewPage() throws Exception {
        Recipe testRecipe = new Recipe(1,"test recipe");
        when(recipeService.show(1)).thenReturn(testRecipe);

        this.mockMvc
                .perform(get("/recipe/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe/edit"))
                .andExpect(model().attributeExists("recipe"))
                .andExpect(model().attributeExists("recipeDetails"));
    }

    @Test
    @WithMockUser(username = "admin", password = "admin")
    void shouldReturnNewRecipeViewPage() throws Exception {
        this.mockMvc
                .perform(get("/recipe/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("recipe/edit"))
                .andExpect(model().attributeExists("recipe"));
    }

    @Test
    void shouldDeleteRecipe() throws Exception {
        this.mockMvc
                .perform(delete("/recipe/1")
                        .with(user("admin"))
                        .with(csrf())
                );
        verify(recipeService).delete(1);
    }
}